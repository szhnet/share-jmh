package share.jmh;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/**
 * @author <a href="mailto:szhnet@gmail.com">szh</a>
 */
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class FloatDoubleBenchmark {

    @State(Scope.Thread)
    public static class ThreadState {

        private float fval = 5.3867f;

        private double dval = 5.3867;

    }

    @Benchmark
    public float floatBaseline(ThreadState state) {
        return state.fval;
    }

    @Benchmark
    public double doubleBaseline(ThreadState state) {
        return state.dval;
    }

    @Benchmark
    public float floatCalc(ThreadState state) {
        float v = state.fval;
        v -= 0.6324f;
        v *= 0.6525f;
        v -= 1.3255f;
        v /= 0.5787f;
        v += 1.1457f;
        v *= 1.2148f;
        v += 0.5477f;
        v /= 1.4777f;
        v -= 0.5478f;
        v *= 0.8477f;
        v -= 0.8516f;
        v /= 1.2565f;
        v += 0.8879f;
        v *= 0.8958f;
        v += 0.5255f;
        v /= 0.9465f;
        v -= 0.9155f;
        v *= 0.6875f;
        v -= 0.5742f;
        v /= 1.3265f;

        return v;
    }

    @Benchmark
    public double doubleCalc(ThreadState state) {
        double v = state.dval;
        v -= 0.6324;
        v *= 0.6525;
        v -= 1.3255;
        v /= 0.5787;
        v += 1.1457;
        v *= 1.2148;
        v += 0.5477;
        v /= 1.4777;
        v -= 0.5478;
        v *= 0.8477;
        v -= 0.8516;
        v /= 1.2565;
        v += 0.8879;
        v *= 0.8958;
        v += 0.5255;
        v /= 0.9465;
        v -= 0.9155;
        v *= 0.6875;
        v -= 0.5742;
        v /= 1.3265;

        return v;
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include("FloatDoubleBenchmark")
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .build();

        new Runner(opt).run();
    }

}
