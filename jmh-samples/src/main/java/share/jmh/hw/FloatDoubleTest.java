package share.jmh.hw;

/**
 * @author <a href="mailto:szhnet@gmail.com">szh</a>
 */
public class FloatDoubleTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        new FloatDoubleTest().test();
    }

    public void test() {
        int warmup = 100000;
        int test = 100000000;
        double d = 5.3867;
        float f = 5.3867f;

        for (int i = 0; i < warmup; i++) {
            testDouble(200, d);
            testFloat(200, f);
        }

        System.out.println("begin test");

        long doubleT1 = System.nanoTime();
        testDouble(test, d);
        long doubleT2 = System.nanoTime();

        long floatT1 = System.nanoTime();
        testFloat(test, f);
        long floatT2 = System.nanoTime();

        System.out.println("double: " + (doubleT2 - doubleT1) / 1000000 + "ms");
        System.out.println("float : " + (floatT2 - floatT1) / 1000000 + "ms");
    }

    public void testDouble(int loop, double d) {
        double v = d;
        for (int i = 0; i < loop; i++) {
            v -= 0.6324;
            v *= 0.6525;
            v -= 1.3255;
            v /= 0.5787;
            v += 1.1457;
            v *= 1.2148;
            v += 0.5477;
            v /= 1.4777;
            v -= 0.5478;
            v *= 0.8477;
            v -= 0.8516;
            v /= 1.2565;
            v += 0.8879;
            v *= 0.8958;
            v += 0.5255;
            v /= 0.9465;
            v -= 0.9155;
            v *= 0.6875;
            v -= 0.5742;
            v /= 1.3265;
        }

        if (v == 1.0) {
            System.out.println("hehe");
        }
    }

    public void testFloat(int loop, float f) {
        float v = f;
        for (int i = 0; i < loop; i++) {
            v -= 0.6324f;
            v *= 0.6525f;
            v -= 1.3255f;
            v /= 0.5787f;
            v += 1.1457f;
            v *= 1.2148f;
            v += 0.5477f;
            v /= 1.4777f;
            v -= 0.5478f;
            v *= 0.8477f;
            v -= 0.8516f;
            v /= 1.2565f;
            v += 0.8879f;
            v *= 0.8958f;
            v += 0.5255f;
            v /= 0.9465f;
            v -= 0.9155f;
            v *= 0.6875f;
            v -= 0.5742f;
            v /= 1.3265f;
        }

        if (v == 1.0) {
            System.out.println("hehe");
        }
    }


}
