package share.jmh.hw;

public class Test {

    public static void main(String argv[]) {
        long t1 = System.currentTimeMillis();
        for (long i = 0; i < 10000000000L; i++) {
            double v = 5.3867;
            v -= 0.6324;
            v *= 0.6525;
            v -= 1.3255;
            v /= 0.5787;
            v += 1.1457;
            v *= 1.2148;
            v += 0.5477;
            v /= 1.4777;
            v -= 0.5478;
            v *= 0.8477;
            v -= 0.8516;
            v /= 1.2565;
            v += 0.8879;
            v *= 0.8958;
            v += 0.5255;
            v /= 0.9465;
            v -= 0.9155;
            v *= 0.6875;
            v -= 0.5742;
            v /= 1.3265;
        }
        long t2 = System.currentTimeMillis();

        long t3 = System.currentTimeMillis();
        for (long i = 0; i < 10000000000L; i++) {
            float v = 5.3867f;
            v -= 0.6324f;
            v *= 0.6525f;
            v -= 1.3255f;
            v /= 0.5787f;
            v += 1.1457f;
            v *= 1.2148f;
            v += 0.5477f;
            v /= 1.4777f;
            v -= 0.5478f;
            v *= 0.8477f;
            v -= 0.8516f;
            v /= 1.2565f;
            v += 0.8879f;
            v *= 0.8958f;
            v += 0.5255f;
            v /= 0.9465f;
            v -= 0.9155f;
            v *= 0.6875f;
            v -= 0.5742f;
            v /= 1.3265f;
        }
        long t4 = System.currentTimeMillis();

        System.out.println("double:" + (t2 - t1) + "ms, float:" + (t4 - t3) + "ms");
    }

}
